// import { shallowMount } from '@vue/test-utils'
// import HelloWorld from '@/components/HelloWorld.vue'

// describe('HelloWorld.vue', () => {
//   it('renders props.msg when passed', () => {
//     const msg = 'new message'
//     const wrapper = shallowMount(HelloWorld, {
//       propsData: { msg }
//     })
//     expect(wrapper.text()).toMatch(msg)
//   })
// })

import { mount } from '@vue/test-utils'
import formComponent from './formComponent.vue';

describe('formComponent', () => {
    it('renders a div', () => {
      const wrapper = mount(formComponent)
      expect(wrapper.contains('div')).toBe(true)
    })
  })

import { mount } from '@vue/test-utils'
import formComponent from './formComponent.vue'

const wrapper = mount(formComponent)
expect(wrapper.classes()).toContain('submit')
expect(wrapper.classes('submit')).toBe(true)
