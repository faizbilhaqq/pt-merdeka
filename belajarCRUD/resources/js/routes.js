import ExampleComponent from "../js/components/ExampleComponent.vue";
import formComponent from "../js/components/formComponent.vue"
import table from "../js/components/table.vue"
import update from "../js/components/update.vue"
import bodyComponent from '../js/components/body.vue';
import portfolio from '../js/components/portfolio.vue'

export const routes = [
    {
        path: '/',
        component: bodyComponent,
        name: "bodyComponent"
    },
    {
        path: '/form',
        component: formComponent,
        name: "formComponent"
    },
    {
        path: '/table',
        component: table,
        name: "table"
    },
    {
        path: '/update/:id',
        component: update,
        name: "update"
    },
    {
        path: '/portfolio',
        component: portfolio,
        name: "portfolio"
    },
    // {
    //     path: '/nav',
    //     component: nav,
    //     name: "nav"
    // },


] 