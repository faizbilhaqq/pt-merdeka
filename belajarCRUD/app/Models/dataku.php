<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class dataku extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "dataku";
    protected $fillable = [
        "name",
        "address",
        "company"
    ];

    protected $hidden = [];
    
}