<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Helpers\apiFormatter;
use App\Http\Controllers\Controller;
use App\Models\dataku;
use Exception;

class api_dataku extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = dataku::all();

        if ($data) {
            return apiFormatter::createApi(200, 'Success', $data);
        } else {
            return apiFormatter::createApi(400, 'Failed');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $request->validate([
                'name' => 'required',
                'address' => 'required',
                'company' => 'required',
            ]);

            $dataku = dataku::create([
                'name' => $request->name,
                'address' => $request->address,
                'company' => $request->company,
            ]);

            $data = dataku::where('id', '=', $dataku->id)->get();

            if ($data) {
                return apiFormatter::createApi(200, 'Success', $data);
            } else {
                return apiFormatter::createApi(400, 'Failed');
            }
        }catch(Exception $error){
            return apiFormatter::createApi(400, 'Failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        {
            $data = dataku::where('id', '=', $id)->get();
    
            if ($data) {
                return apiFormatter::createApi(200, 'Success', $data);
            } else {
                return apiFormatter::createApi(400, 'Failed');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        {
            try {
                $request->validate([
                    'name' => 'required',
                'address' => 'required',
                'company' => 'required',
                ]);
    
    
                $dataku = dataku::findOrFail($id);
    
                $dataku->update([
                    'name' => $request->name,
                    'address' => $request->address,
                    'company' => $request->company,
                ]);
    
                $data = dataku::where('id', '=', $dataku->id)->get();
    
                if ($data) {
                    return apiFormatter::createApi(200, 'Success', $data);
                } else {
                    return apiFormatter::createApi(400, 'Failed');
                }
            } catch (Exception $error) {
                return apiFormatter::createApi(400, 'Failed');
            }
    }}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * A @return \Illuminate\Http\Response

     */
    public function destroy($id)
    {
        try {
            $dataku = dataku::findOrFail($id);

            $data = $dataku->delete();

            if ($data) {
                return apiFormatter::createApi(200, 'Success Destory data');
            } else {
                return apiFormatter::createApi(400, 'Failed');
            }
        } catch (Exception $error) {
            return apiFormatter::createApi(400, 'Failed');
        }
    }
}


