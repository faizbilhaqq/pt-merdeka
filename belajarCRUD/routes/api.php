<?php

use App\Http\Controllers\API\api_dataku;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('dataku', [api_dataku::class, 'index']);
Route::post('dataku/store', [api_dataku::class, 'store']);   
Route::get('dataku/show/{id}', [api_dataku::class, 'show']);
Route::post('dataku/update/{id}', [api_dataku::class, 'update']);
Route::delete('dataku/destroy/{id}', [api_dataku::class, 'destroy']);






// Route::get('/dataku', 'App\Http\Controllers\api_dataku@index');

// Route::middleware('api')->group(function () {
//     Route::resource('dataku', ProductController::class);
// });


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
