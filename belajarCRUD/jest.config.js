module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/no-babel',
  testEnvironment: 'node',

  cache: false,

  testMatch: [
    '**/tests/**/*.spec.(js|jsx|ts|tsx)|**/__tests__/*.(js|jsx|ts|tsx)'
  ],
 
  // testEnvironment: "jest-environment-jsdom",

  moduleFileExtensions: [
    "js",
    "ts",
    "vue"
  ],
}
